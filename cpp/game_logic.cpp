#include "game_logic.h"
#include "protocol.h"



using namespace hwo_protocol;

game_logic::game_logic()  
{
	action_map.emplace("join",			std::mem_fn(&game_logic::on_join));
	action_map.emplace("gameInit", std::mem_fn(&game_logic::on_game_init));
	action_map.emplace("gameStart",		std::mem_fn(&game_logic::on_game_start));
	action_map.emplace("carPositions",	std::mem_fn(&game_logic::on_car_positions));
	action_map.emplace("crash",			std::mem_fn(&game_logic::on_crash));
	action_map.emplace("gameEnd",		std::mem_fn(&game_logic::on_game_end));
	action_map.emplace("error",			std::mem_fn(&game_logic::on_error));
	action_map.emplace("spawn", std::mem_fn(&game_logic::on_spawn));
	action_map.emplace("yourCar", std::mem_fn(&game_logic::on_your_car));
	action_map.emplace("lapFinished", std::mem_fn(&game_logic::on_lap_finished));
	action_map.emplace("dnf", std::mem_fn(&game_logic::on_dnf));
	action_map.emplace("turboAvailable", std::mem_fn(&game_logic::on_turboAvailable));
	action_map.emplace("turboStart", std::mem_fn(&game_logic::on_turboStart));
	action_map.emplace("turboEnd", std::mem_fn(&game_logic::on_turboEnd));
	
}

game_logic::msg_vector game_logic::react(const jsoncons::json& msg)
{
  const auto& msg_type = msg["msgType"].as<std::string>();
  const auto& data = msg["data"];
  int gameTick = -1;
  if (msg.has_member("gameTick"))
  {
	  gameTick = msg["gameTick"].as_longlong();
  }
  auto action_it = action_map.find(msg_type);

  if (action_it != action_map.end())
  {
    return (action_it->second)(this, data, gameTick);
  }
  else
  {
    std::cout << "Unknown message type: " << msg_type << std::endl;
	std::cout << jsoncons::pretty_print(data);
    return { make_ping() };
  }
}

game_logic::msg_vector game_logic::on_join(const jsoncons::json& data, int gameTick)
{
  std::cout << "Joined" << std::endl;
  return { make_ping() };
}

game_logic::msg_vector game_logic::on_game_start(const jsoncons::json& data, int gameTick)
{
  std::cout << "Race started" << std::endl;
  return pCarBot->OnGameStart(data);
}

game_logic::msg_vector game_logic::on_car_positions(const jsoncons::json& data, int gameTick)
{
	return pCarBot->OnCarPositions(data, gameTick);
}

game_logic::msg_vector game_logic::on_crash(const jsoncons::json& data, int gameTick)
{
  std::cout << "Someone crashed" << data << std::endl;
  return pCarBot->OnCarCrash(data, gameTick);
}

game_logic::msg_vector game_logic::on_game_end(const jsoncons::json& data, int gameTick)
{
  std::cout << "Race ended" << std::endl;
  return { make_ping() };
}

game_logic::msg_vector game_logic::on_error(const jsoncons::json& data, int gameTick)
{
  std::cout << "Error: " << data.to_string() << std::endl;
  return { make_ping() };
}

game_logic::msg_vector game_logic::on_spawn(const jsoncons::json& data, int gameTick)
{
	std::cout << "Spawned: " << data.to_string() << std::endl;
	return pCarBot->OnCarSpawn(data, gameTick);
}

game_logic::msg_vector game_logic::on_your_car(const jsoncons::json& data, int gameTick)
{
	myCarColor = data["color"].as_string();
	
	std::cout << "Your car: " << myCarColor << std::endl;
	return{ make_ping() };
}

game_logic::msg_vector game_logic::on_lap_finished(const jsoncons::json& data, int gameTick)
{
	std::cout << "LapFinished: " << data.to_string() << std::endl;
	return pCarBot->OnLapFinished(data, gameTick);
}

game_logic::msg_vector game_logic::on_game_init(const jsoncons::json& data, int gameTick)
{
	std::cout << "GameInit: " << std::endl;
	
	json_vector pieces, lanes;

	auto &trackPieces = data["race"]["track"]["pieces"];
	for (auto it = trackPieces.begin_elements(); it != trackPieces.end_elements(); ++it)
		pieces.push_back(*it);

	auto &trackLanes = data["race"]["track"]["lanes"];
	for (auto it = trackLanes.begin_elements(); it != trackLanes.end_elements(); ++it)
		lanes.push_back(*it);

	auto &cars = data["race"]["cars"];
	double carLength = 0, carWidth = 0, carGuideFlagPos = 0;
	for (auto it = cars.begin_elements(); it != cars.end_elements(); ++it)
	{
		if ((*it)["id"]["color"].as_string() == myCarColor)
		{
			auto &dim = (*it)["dimensions"];
			carLength = dim["length"].as_double();
			carWidth = dim["width"].as_double();
			carGuideFlagPos = dim["guideFlagPosition"].as_double();
			break;
		}
	}

	if (!pCarBot)
	{
		pCarBot.reset(new CarBotBase(myCarColor));
		int numLaps = 0;
		auto &race = data["race"];
		if (race.has_member("raceSession"))
		{
			auto &raceSession = race["raceSession"];
			if (raceSession.has_member("laps"))
			{
				std::cout << "------------- Race session!" << std::endl;
				numLaps = raceSession["laps"].as_longlong();
				pCarBot->SetQualifying(false);
			}
			else
			{
				std::cout << "------------- Qualifying session!" << std::endl;
				pCarBot->SetQualifying(true);
			}
		}

		pCarBot->SetNumLaps(numLaps);
	}

	if (!pTrackInfo)
	{
		pTrackInfo = std::make_shared<TrackInfo>(pieces, lanes, carLength, carWidth, carGuideFlagPos);
		pCarBot->SetTrackInfo(pTrackInfo);
		
	}

	
	
	return{ make_ping() };
}

game_logic::msg_vector game_logic::on_dnf(const jsoncons::json& data, int gameTick)
{
	std::cout << "Disqualified: " << data["car"]["color"].to_string() << "reason: " << data["reason"].to_string() << std::endl;
	return{ make_ping() };
}

msg_vector game_logic::on_turboAvailable(const jsoncons::json& data, int gameTick)
{
	std::cout << "Turbo available!" << std::endl;
	return pCarBot->OnTurboAvailable(data);
}

msg_vector game_logic::on_turboStart(const jsoncons::json& data, int gameTick)
{
	std::cout << "Turbo start!" << data << std::endl;
	return pCarBot->OnTurboStart(data);
}

msg_vector game_logic::on_turboEnd(const jsoncons::json& data, int gameTick)
{
	std::cout << "Turbo end!" << data << std::endl;
	return pCarBot->OnTurboEnd(data);
}