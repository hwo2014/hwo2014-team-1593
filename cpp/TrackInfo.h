#ifndef HWO_TRACKINFO_H
#define HWO_TRACKINFO_H

#include <iostream>
#include <vector>
#include <utility>
#include <map>
#include <jsoncons/json.hpp>

typedef std::vector<jsoncons::json> json_vector;

class TrackInfo
{
public:
	typedef int TrackPieceIndex;
	typedef int TrackLaneIndex;
	typedef std::pair<TrackPieceIndex, TrackLaneIndex> TrackNode;
	typedef std::pair<TrackNode, TrackNode> TrackNodePair;
	typedef std::vector<TrackNode> TrackPath;

	enum class PieceType
	{
		straight,
		shortTurn,
		longTurn,
		longTightTurn,
		shortTightTurn
	};

	TrackInfo(json_vector trackPieces, json_vector lanes, double carLength, double carWidth, double carGuideFlagPos);

	TrackPath GetShortestPath(TrackNode start, TrackNode end) const;

	static void PrintPath(const TrackPath &path);

	int numPieces, numLanes;
	std::vector<PieceType> pieceTypes;
	std::vector <std::vector<double>> pieceLengths;
	std::vector <std::vector<double>> speedLimits;
	std::map<TrackNodePair, double> shortestDistance;
	std::map<TrackNodePair, TrackNode> closestPredecessor;
};


#endif