#include "CarBotBase.h"
#include "TrackInfo.h"
#include "protocol.h"

#include <iomanip>

using namespace hwo_protocol;

CarBotBase::CarBotBase(const std::string &color)
	: color{color}
{}

msg_vector CarBotBase::OnCarCrash(const jsoncons::json& data, int gameTick)
{
	return{ make_ping() };
}

msg_vector CarBotBase::OnCarSpawn(const jsoncons::json& data, int gameTick)
{
	return{ make_ping() };
}

msg_vector CarBotBase::OnLapFinished(const jsoncons::json& data, int gameTick)
{
	++currentLap;
	return{ make_ping() };
}

msg_vector CarBotBase::OnTurboAvailable(const jsoncons::json& data)
{
	bTurboAvailable = true;
	return{ make_ping() };
}

msg_vector CarBotBase::OnTurboStart(const jsoncons::json& data)
{
	bTurboActive = true;
	return{ make_ping() };
}

msg_vector CarBotBase::OnTurboEnd(const jsoncons::json& data)
{
	bTurboActive = false;
	return{ make_ping() };
}

msg_vector CarBotBase::OnGameStart(const jsoncons::json& data)
{
	currentLap = 1;
	return{ make_ping() };
}

msg_vector CarBotBase::OnCarPositions(const jsoncons::json &data, int gameTick)
{
	currentGameTick = gameTick;

	if (currentGameTick < 0)
		return{ make_throttle(0.0) };

	if (bTurboActive)
		return{ make_throttle(1.0) };

	for (auto it = data.begin_elements(); it != data.end_elements(); ++it)
	{
		if ((*it)["id"]["color"] == color)
		{
			// this is our car

			auto &carInfo = *it;
			currentTrackPiece = carInfo["piecePosition"]["pieceIndex"].as_longlong();
			currentLane = carInfo["piecePosition"]["lane"]["endLaneIndex"].as_longlong();
			inPiecePosition = carInfo["piecePosition"]["inPieceDistance"].as_double();
			currentCarAngle = carInfo["angle"].as_double();

			TrackInfo::TrackNode startNode = std::make_pair(currentTrackPiece + 1, currentLane);
			TrackInfo::TrackNode endNode = std::make_pair(pTrackInfo->numPieces - 1, pTrackInfo->numLanes - 1);
			auto shortestPath = pTrackInfo->GetShortestPath(startNode, endNode);

			// Check if we're on the correct lane and if we are not then issue a switch command
			for (int i = 0; i < 3 && i < shortestPath.size(); ++i)
			{
				if (currentLane != shortestPath[i].second
					&& desiredLaneIndex != shortestPath[i].second)
				{
					desiredLaneIndex = shortestPath[i].second;
					return{ make_switch(currentLane < desiredLaneIndex ? "Right" : "Left") };
				}
			}
		}
	}

	UpdateCarInfo();

	if (!bFinishedDataAquisition)
		AcquireTrackData();
	else
	{
		throttle = 1.0;
	
		// Reduce speed if a turn is coming
		if (ShouldSlowDown())
			throttle = 0.0;
		else if (!bQualifying && currentLap == numLaps && StraighLineToFinish() && bTurboAvailable)
			return { make_turbo()};
	}
	return{ make_throttle(throttle) };
}

void CarBotBase::AcquireTrackData()
{
	// take position samples at the beginning of the first lap
	throttle = 1.0;
	if (currentGameTick >= sampleRisingStartTick && currentGameTick <= sampleRisingEndTick)
	{
		throttle = 1.0;
		positionSamples.push_back(currentPosition);
	}
	else if (currentGameTick >= sampleFallingStartTick && currentGameTick <= sampleFallingEndTick)
	{
		throttle = 0.0;
		positionSamples.push_back(currentPosition);
	}
	else if (currentGameTick == sampleFallingEndTick + 1)
		AnalyzeSamples();
	else
	{
		if (currentGameTick > sampleFallingEndTick + 1)
			bFinishedDataAquisition = true;
	}
}

bool CarBotBase::ShouldSlowDown()
{
	/*
	double speedLimitInTurn = 6.5;
	for (int i = currentTrackPiece; i < pTrackInfo->numPieces * 2; ++i)
	{
		int index = i % pTrackInfo->numPieces;
		if (pTrackInfo->pieceTypes[index] != TrackInfo::PieceType::straight)
		{
			speedLimitInTurn = pTrackInfo->speedLimits[index][currentLane];
			std::cout << "ct = " << index << " csl = " << speedLimitInTurn << " ";
			break;
		}
	}


	// fallingAcceleration 0 or positive??
	if (fallingAcceleration > -1e-6 || currentSpeed < speedLimitInTurn)
		return false;

	double timeLeftBeforeSlowdown = GetTimeWithNoThrottleToReachNextTurnWithVelocity(speedLimitInTurn);
	if (timeLeftBeforeSlowdown < 16)
	{
		std::cout << " Slowing down..." << std::endl;
		return true;
	}
	
	
	
	
	return false;
	*/



	// Reduce speed if a turn is coming
	int lookAhead = 2;
	if (bLookAheadAfterTurbo)
	{
		bLookAheadAfterTurbo = false;
		lookAhead = 5;
	}

	int n = pTrackInfo->numPieces;
	if (currentSpeed > 4.5)
	{
		for (int i = currentTrackPiece; i < currentTrackPiece + lookAhead; ++i)
			if (pTrackInfo->pieceTypes[i % n] == TrackInfo::PieceType::longTightTurn)
			{
				std::cout << "Slowing down..." << std::endl;
				return true;
			}
	}
	if (currentSpeed > 6.30)
	{
		for (int i = currentTrackPiece; i < currentTrackPiece + lookAhead; ++i)
			if (pTrackInfo->pieceTypes[i % n] == TrackInfo::PieceType::longTurn)
			{
				std::cout << "Slowing down..." << std::endl;
				return true;
			}
	}
	if (currentSpeed > 8.0)
	{
		for (int i = currentTrackPiece; i < currentTrackPiece + lookAhead; ++i)
			if (pTrackInfo->pieceTypes[i % n] == TrackInfo::PieceType::shortTightTurn)
			{
				std::cout << "Slowing down..." << std::endl;
				return true;
			}
	}
	if (currentSpeed > 10.2)
	{
		for (int i = currentTrackPiece; i < currentTrackPiece + lookAhead; ++i)
			if (pTrackInfo->pieceTypes[i % n] == TrackInfo::PieceType::shortTurn)
			{
				std::cout << "Slowing down..." << std::endl;
				return true;
			}
	}

	return false;
}

void CarBotBase::AnalyzeSamples()
{
	{
		int risingEnd = sampleRisingEndTick - sampleRisingStartTick;
		std::vector<double> speedSamples;
		for (int i = 0; i < risingEnd - 1; ++i)
		{
			speedSamples.push_back(positionSamples[i + 1] - positionSamples[i]);
		}
		std::vector<double> accelSamples;
		risingAcceleration = 0.0;
		for (int i = 0; i < speedSamples.size() - 1; ++i)
		{
			accelSamples.push_back(speedSamples[i + 1] - speedSamples[i]);
			risingAcceleration += accelSamples.back();
		}
		risingAcceleration /= speedSamples.size();
	}
	{
		int fallingStart = sampleRisingEndTick - sampleRisingStartTick + 1;
		int fallingEnd = sampleFallingEndTick - sampleFallingStartTick + 1 + fallingStart;
		std::vector<double> speedSamples;
		for (int i = fallingStart; i < fallingEnd - 1; ++i)
		{
			speedSamples.push_back(positionSamples[i + 1] - positionSamples[i]);
		}
		std::vector<double> accelSamples;
		fallingAcceleration = 0.0;
		for (int i = 0; i < speedSamples.size() - 1; ++i)
		{
			accelSamples.push_back(speedSamples[i + 1] - speedSamples[i]);
			fallingAcceleration += accelSamples.back();
		}
		fallingAcceleration /= speedSamples.size();
	}
}

double CarBotBase::GetTimeWithNoThrottleToReachNextTurnWithVelocity(double desiredVelocity)
{
	double v = desiredVelocity;
	double d = GetDistanceUntilNextTurn();
	double a = fallingAcceleration;

	// solve d = vt - (at^2)/2 for t
	double sqrtQuantity = v * v - 2 * a * d;	
	if (sqrtQuantity < 0)
		return -1;
	double sqrt_b2_4ac = sqrt(sqrtQuantity);
	double t1 = (v - sqrt_b2_4ac) / a;
	std::cout.precision(5);
	//std::cout << " \td=" << d << " \tt1=" << t1 << std::endl;

	return t1 + 1;
}

double CarBotBase::GetDistanceUntilNextTurn()
{
	if (pTrackInfo->pieceTypes[currentTrackPiece] != TrackInfo::PieceType::straight)
	{
		return 0.0;
	}

	// straight line distance for the current piece
	double distance = pTrackInfo->pieceLengths[currentTrackPiece][currentLane] - inPiecePosition;

	for (int i = currentTrackPiece + 1; i < pTrackInfo->numPieces * 2; ++i)
	{
		if (pTrackInfo->pieceTypes[i % pTrackInfo->numPieces] == TrackInfo::PieceType::straight)
			distance += pTrackInfo->pieceLengths[i % pTrackInfo->numPieces][currentLane];
		else
			break;
	}

	return distance;
}

bool CarBotBase::StraighLineToFinish()
{
	for (int i = currentTrackPiece; i < pTrackInfo->numPieces; ++i)
		if (pTrackInfo->pieceTypes[i] != TrackInfo::PieceType::straight)
			return false;
	return true;
}

void CarBotBase::UpdateCarInfo()
{
	// Update car position and speed
	auto oldCarPosition = currentPosition;
	currentPosition = 0;
	for (int i = 0; i < currentTrackPiece; ++i)
	{
		currentPosition += pTrackInfo->pieceLengths[i][currentLane];
	}
	if (pTrackInfo->pieceTypes[currentTrackPiece] != TrackInfo::PieceType::straight)
	{
		double laneInPiecePos = inPiecePosition;//* (pTrackInfo->pieceLengths[currentTrackPiece][currentLane] / pTrackInfo->pieceLengths[currentTrackPiece][pTrackInfo->numLanes]);
		currentPosition += laneInPiecePos;
	}
	else
	{
		currentPosition += inPiecePosition;
	}
	currentSpeed = currentPosition - oldCarPosition;

	if (currentSpeed > 0.01)
	{
		std::cout.precision(5);
		std::cout << "[" << std::setw(5) << currentGameTick << "] s:" << currentSpeed
			<< "  \tp:" << currentPosition
			<< "  \ta:" << currentCarAngle << std::endl;
	}
}