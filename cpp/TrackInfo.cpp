#include "TrackInfo.h"

#include <boost/graph/graph_traits.hpp>
#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/dijkstra_shortest_paths.hpp>
#include <boost/property_map/property_map.hpp>
#include <algorithm>

using namespace boost;

TrackInfo::TrackInfo(json_vector trackPieces, json_vector lanes, double carLength, double carWidth, double carGuideFlagPos)
{
	numPieces = trackPieces.size();
	numLanes = lanes.size();
	for (int i = 0; i < trackPieces.size(); ++i)
	{
		pieceLengths.emplace_back();
		speedLimits.emplace_back();
		if (trackPieces[i].has_member("radius"))
		{
			auto angle = trackPieces[i]["angle"].as_double();
			auto radius = trackPieces[i]["radius"].as_double();
			double pi = 3.141592;
			for (int j = 0; j < lanes.size(); ++j)
			{
				auto distanceFromCenter = lanes[j]["distanceFromCenter"].as_longlong();
				auto r = radius - (angle >= 0 ? distanceFromCenter : -distanceFromCenter);
				pieceLengths[i].push_back(r * pi * abs(angle) / 180.0);
				double slipAngle = pi / 4.0;
				double carFlagOffset = carLength / 2.0 - carGuideFlagPos;
				double limit = 3 * sqrt((slipAngle * carFlagOffset * carWidth * carLength) / (r * pieceLengths[i].back()));
				speedLimits[i].push_back(limit);

			}
			// keep the distance through the center of the piece at numLanes+1
			pieceLengths[i].push_back(radius * pi * abs(angle) / 180);
			if (abs(angle) < 30)
			{
				if (radius < 75)
					pieceTypes.push_back(PieceType::shortTightTurn);
				else
					pieceTypes.push_back(PieceType::shortTurn);

			}
			else
			{
				if (radius < 75)
					pieceTypes.push_back(PieceType::longTightTurn);
				else
					pieceTypes.push_back(PieceType::longTurn);
			}

		}
		else
		{
			for (int j = 0; j < lanes.size(); ++j)
			{
				pieceLengths[i].push_back(trackPieces[i]["length"].as_double());
				speedLimits[i].push_back(20.0);	// hardcoded max speed
			}
			pieceTypes.push_back(PieceType::straight);
		}
	}

	typedef adjacency_list < listS, vecS, directedS,
		no_property, property < edge_weight_t, int > > graph_t;
	typedef graph_traits < graph_t >::vertex_descriptor vertex_descriptor;
	typedef std::pair<int, int> Edge;
	std::vector<Edge> edgeArray;
	std::vector<double> lengths;

	std::vector<std::pair<int, int>> indexToPair;
	std::vector<std::vector<int>> pairToIndex;
	for (int i = 0; i < pieceLengths.size(); ++i)
	{
		pairToIndex.emplace_back();
		for (int j = 0; j < lanes.size(); ++j)
		{
			indexToPair.push_back(std::make_pair(i, j));
			pairToIndex[i].push_back(indexToPair.size() - 1);
		}
	}


	for (int i = 0; i < pieceLengths.size() - 1; ++i)
	{
		for (int j = 0; j < lanes.size(); ++j)
		{
			edgeArray.emplace_back(pairToIndex[i][j], pairToIndex[i + 1][j]);
			lengths.push_back(pieceLengths[i][j]);

		}
		if (trackPieces[i].has_member("switch"))
		{
			for (int j = 0; j < lanes.size() - 1; ++j)
			{
				edgeArray.emplace_back(pairToIndex[i][j], pairToIndex[i + 1][j + 1]);
				lengths.push_back(pieceLengths[i][j]);
				edgeArray.emplace_back(pairToIndex[i][j + 1], pairToIndex[i + 1][j]);
				lengths.push_back(pieceLengths[i][j]);

			}
		}
	}
	for (int j = 0; j < lanes.size(); ++j)
	{
		edgeArray.emplace_back(pairToIndex[pieceLengths.size() - 1][j], pairToIndex[0][j]);
		lengths.push_back(pieceLengths[pieceLengths.size() - 1][j]);
	}
	if (trackPieces[trackPieces.size() - 1].has_member("switch"))
	{
		for (int j = 0; j < lanes.size() - 1; ++j)
		{
			edgeArray.emplace_back(pairToIndex[trackPieces.size() - 1][j], pairToIndex[0][j + 1]);
			lengths.push_back(pieceLengths[pieceLengths.size() - 1][j]);
			edgeArray.emplace_back(pairToIndex[trackPieces.size() - 1][j + 1], pairToIndex[0][j]);
			lengths.push_back(pieceLengths[pieceLengths.size() - 1][j]);
		}
	}

	graph_t g(edgeArray.begin(), edgeArray.end(), lengths.begin(), indexToPair.size());
	property_map<graph_t, edge_weight_t>::type weightmap = get(edge_weight, g);
	std::vector<vertex_descriptor> p(num_vertices(g));
	std::vector<int> d(num_vertices(g));

	for (int i = 0; i < trackPieces.size(); ++i)
	{
		for (int j = 0; j < lanes.size(); ++j)
		{
			vertex_descriptor s = boost::vertex(pairToIndex[i][j], g);

			dijkstra_shortest_paths(g, s,
				predecessor_map(boost::make_iterator_property_map(p.begin(), get(boost::vertex_index, g))).
				distance_map(boost::make_iterator_property_map(d.begin(), get(boost::vertex_index, g))));

			// fill up shortestDistances and closestPredecessor vectors
			graph_traits < graph_t >::vertex_iterator vi, vend;
			for (boost::tie(vi, vend) = vertices(g); vi != vend; ++vi)
			{
				shortestDistance[std::make_pair(std::make_pair(i, j), indexToPair[*vi])] = d[*vi];
				closestPredecessor[std::make_pair(std::make_pair(i, j), indexToPair[*vi])] = indexToPair[p[*vi]];
			}
		}
	}
}

TrackInfo::TrackPath TrackInfo::GetShortestPath(TrackNode start, TrackNode end) const
{
	if (start.first >= numPieces || end.first >= numPieces)
		return TrackPath();
	TrackPath path;
	path.push_back(end);
	for (int i = 0; i < numPieces; ++i)
	{
		path.push_back(closestPredecessor.at({start, path.back()}));
		if (path.back() == start)
			break;
	}
	if (path.back() != start)
		return TrackPath();

	std::reverse(path.begin(), path.end());

	return path;
}

void TrackInfo::PrintPath(const TrackInfo::TrackPath &path)
{
	std::cout << "{";
	for (auto &e : path)
	{
		std::cout << "(" << e.first << "," << e.second << ")";
	}
	std::cout << "}" << std::endl;
}