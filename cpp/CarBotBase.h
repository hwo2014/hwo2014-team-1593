#ifndef HWO_CARBOTBASE_H
#define HWO_CARBOTBASE_H

#include "TrackInfo.h"

#include <vector>
#include <jsoncons/json.hpp>


typedef std::vector<jsoncons::json> msg_vector;

class CarBotBase
{
public:

	CarBotBase(const std::string &color);

	msg_vector OnCarPositions(const jsoncons::json &data, int gameTick);
	msg_vector OnCarCrash(const jsoncons::json& data, int gameTick);
	msg_vector OnCarSpawn(const jsoncons::json& data, int gameTick);
	msg_vector OnLapFinished(const jsoncons::json& data, int gameTick);
	msg_vector OnTurboAvailable(const jsoncons::json& data);
	msg_vector OnTurboStart(const jsoncons::json& data);
	msg_vector OnTurboEnd(const jsoncons::json& data);
	msg_vector OnGameStart(const jsoncons::json& data);


	void SetTrackInfo(std::shared_ptr<TrackInfo> pTrackInfo) { this->pTrackInfo = pTrackInfo; }

	void SetNumLaps(int numLaps) { this->numLaps = numLaps; }
	void SetQualifying(bool bQualifying) { this->bQualifying = bQualifying; }

private:
	void AnalyzeSamples();
	double GetTimeWithNoThrottleToReachNextTurnWithVelocity(double desiredVelocity);
	double GetDistanceUntilNextTurn();
	bool ShouldSlowDown();
	bool StraighLineToFinish();
	void UpdateCarInfo();
	void AcquireTrackData();

private:
	bool bQualifying = false;
	bool bFinishedDataAquisition = false;
	double throttle = 0.0;

	double currentPosition;
	double currentSpeed;
	double currentAcceleration;
	double currentCarAngle;
	int currentGameTick = 0;
	int currentLane;
	int currentLap = 0;

	int currentTrackPiece = 0;
	double inPiecePosition = 0;

	std::string color;
	int desiredLaneIndex = 0;
	bool bTurboAvailable = false;
	bool bLookAheadAfterTurbo = false;
	bool bTurboActive = false;

	int numLaps = 0;

	std::shared_ptr<TrackInfo> pTrackInfo;

	std::vector<double> positionSamples;

	const int sampleRisingStartTick = 10;
	const int sampleRisingEndTick = 20;
	const int sampleFallingStartTick = 25;// should be at least two ticks away from rising end
	const int sampleFallingEndTick = 40;

	double risingAcceleration = 0;
	double fallingAcceleration = 0;
};

#endif