#include "protocol.h"

namespace hwo_protocol
{

  jsoncons::json make_request(const std::string& msg_type, const jsoncons::json& data)
  {
    jsoncons::json r;
    r["msgType"] = msg_type;
    r["data"] = data;
    return r;
  }

  jsoncons::json make_join(const std::string& name, const std::string& key)
  {
    jsoncons::json data;
    data["name"] = name;
    data["key"] = key;
    return make_request("join", data);
  }

  jsoncons::json make_switch(const std::string& direction)
  {
	  return make_request("switchLane", direction);
  }

  jsoncons::json make_ping()
  {
    return make_request("ping", jsoncons::null_type());
  }

  jsoncons::json make_throttle(double throttle)
  {
    return make_request("throttle", throttle);
  }

  jsoncons::json make_turbo()
  {
	  return make_request("turbo", "Turbo: Engage!");
  }

  jsoncons::json make_raceRequest(const std::string &name, const std::string &key, const std::string &trackName, const std::string &password, int numCars)
  {
	  jsoncons::json botId;
	  botId["name"] = name;
	  botId["key"] = key;
	  jsoncons::json data;
	  data["botId"] = botId;
	  data["trackName"] = trackName;
	  data["password"] = password;
	  data["carCount"] = numCars;
	  return  make_request("createRace", data);
  }

  jsoncons::json make_joinRequest(const std::string &name, const std::string &key, const std::string &trackName, const std::string &password, int numCars)
  {
	  jsoncons::json botId;
	  botId["name"] = name;
	  botId["key"] = key;
	  jsoncons::json data;
	  data["botId"] = botId;
	  data["trackName"] = trackName;
	  data["password"] = password;
	  data["carCount"] = numCars;
	  return make_request("joinRace", data);
  }

  jsoncons::json make_joinMatch(const std::string &name, const std::string &key, int numCars)
  {
	  jsoncons::json botId;
	  botId["name"] = name;
	  botId["key"] = key;
	  jsoncons::json data;
	  data["botId"] = botId;
	  data["carCount"] = numCars;
	  return make_request("joinRace", data);
  }

}  // namespace hwo_protocol
