#ifndef HWO_GAME_LOGIC_H
#define HWO_GAME_LOGIC_H

#include "TrackInfo.h"
#include "CarBotBase.h"

#include <string>
#include <vector>
#include <map>
#include <functional>
#include <iostream>
#include <jsoncons/json.hpp>
#include <memory>

class game_logic
{
public:
  typedef std::vector<jsoncons::json> msg_vector;

  game_logic();
  msg_vector react(const jsoncons::json& msg);

private:
  typedef std::function<msg_vector(game_logic*, const jsoncons::json&, int)> action_fun;
  std::map<std::string, action_fun> action_map;

  msg_vector on_join(const jsoncons::json& data, int gameTick);
  msg_vector on_game_init(const jsoncons::json& data, int gameTick);
  msg_vector on_game_start(const jsoncons::json& data, int gameTick);
  msg_vector on_car_positions(const jsoncons::json& data, int gameTick);
  msg_vector on_crash(const jsoncons::json& data, int gameTick);
  msg_vector on_game_end(const jsoncons::json& data, int gameTick);
  msg_vector on_error(const jsoncons::json& data, int gameTick);
  msg_vector on_spawn(const jsoncons::json& data, int gameTick);
  msg_vector on_your_car(const jsoncons::json& data, int gameTick);
  msg_vector on_lap_finished(const jsoncons::json& data, int gameTick);
  msg_vector on_dnf(const jsoncons::json& data, int gameTick);
  msg_vector on_turboAvailable(const jsoncons::json& data, int gameTick);
  msg_vector on_turboStart(const jsoncons::json& data, int gameTick);
  msg_vector on_turboEnd(const jsoncons::json& data, int gameTick);


  std::string myCarColor;
  
  std::unique_ptr<CarBotBase> pCarBot;
  std::shared_ptr<TrackInfo> pTrackInfo;
};

#endif
