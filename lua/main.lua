local socket = require "socket"
local json = require "json"

local DEBUG = false


local function LOGD(msg, params)
   if DEBUG then
      print(string.format(msg, params))
   end
end

local NoobBot = {}
NoobBot.__index = NoobBot

function NoobBot.create(conn, name, key, trackname)
   local bot = {}
   setmetatable(bot, NoobBot)
   bot.conn = conn
   bot.name = name
   bot.key = key
   bot.trackname = trackname
   return bot
end

function NoobBot:msg(msgtype, data)
   return json.encode({msgType = msgtype, data = data , gameTick = _gameTick})
end

function NoobBot:send(msg)
   LOGD("Sending msg: %s", msg)
   self.conn:send(msg .. "\n")
end

function NoobBot:join()
	if self.trackname == nil	then
		return self:msg("join", {name = self.name, key = self.key,trackName = self.trackname})
	end

	-- advanced connection
	local ID = {name = self.name, key = self.key}
	return self:msg("joinRace", {botId = ID, trackName = self.trackname,  carCount = 1})
end




function NoobBot:throttle(throttle)
   return self:msg("throttle", throttle)
end

function NoobBot:switchLane(direction)
   return self:msg("switchLane", direction)
end


function NoobBot:ping()
   return self:msg("ping", {})
end

function NoobBot:run()
   self:send(self:join())
   self:msgloop()
end

function NoobBot:onjoin(data)
   print("Joined")
   self:send(self:ping())
end

function NoobBot:ongamestart(data)
   print("Race started")
   self:send(self:ping())
end


function NoobBot:oncrash(data)
   print("Someone crashed")
   self:send(self:ping())
end

function NoobBot:ongameend(data)
   print("Race ended")
   self:send(self:ping())
end

function NoobBot:onerror(data)
   print("Error: " .. data)
   self:send(self:ping())
end

--****************************************************************
_Car = false
_Game = false
_TrackPieces = false
_TrackLanes = false
_SwitchesCommands = false
_CarInfo = {["pieceIndex"] = false ,["inPieceDistance"] = false , ["startLane"] = false , ["endLane"] = false ,  ["lap"] = false , ["angle"] = false }
_gameTick = false


local function GetSpaces(i)
	local s= " "
	for sp=1,i do
		s = s.. s
	end
	return s
end

local offsetIndex = 0

 function PrintTable(k,v)
	offsetIndex = offsetIndex + 1

	local offset = ""
	offset = GetSpaces(offsetIndex)
	
   if type(v) ~= "table" then
		print(offset..tostring(k).." "..tostring(v))
   else
		print(offset..k)
		table.foreach(v ,PrintTable)
   end

   offsetIndex = offsetIndex - 1
   
end

function NoobBot:updateCarInfo(data)
   for _,carInfo in pairs(data) do
		if carInfo["id"]["color"] == _Car["color"] then --my car
			_CarInfo["angle"] = carInfo["angle"]
			_CarInfo["pieceIndex"] = carInfo["piecePosition"]["pieceIndex"]
			_CarInfo["inPieceDistance"] = carInfo["piecePosition"]["inPieceDistance"]
			_CarInfo["startLane"] = carInfo["piecePosition"]["lane"]["startLaneIndex"]
			_CarInfo["endLane"] = carInfo["piecePosition"]["lane"]["endLaneIndex"]
			_CarInfo["lap"] = carInfo["piecePosition"]["lap"]
			
			return
		end
	end
end


lastDistance = false
lastIndex= 0
lastTrackLength = nil
speed = 0

function NoobBot:UpdateCarSpeed()
	
	local index = _CarInfo["pieceIndex"] + 1
	
	if lastDistance  then
		if lastIndex == index then		
		   speed = _CarInfo["inPieceDistance"] - lastDistance 
		else
			speed = lastTrackLength - lastDistance + _CarInfo["inPieceDistance"] 
		end
		print("Speed: "..speed)
	end

	lastTrackLength = _TrackPieces[index]["length"]
	if lastTrackLength == nil then
		local lane = _CarInfo["endLane"] + 1
		local coef = 1 
		if _TrackPieces[index]["angle"] > 0 then
		 coef = -1
		end
		
		lastTrackLength = 0.0174532925 *  math.abs(_TrackPieces[index]["angle"]) * (_TrackPieces[index]["radius"] + (coef *_TrackLanes[lane]["distanceFromCenter"]))
	end
	
	lastDistance = _CarInfo["inPieceDistance"] 
	lastIndex = index
	

end

function NoobBot:SwitchLaneIfCase( forward )

		local index = _CarInfo["pieceIndex"] + 1

		_TrackPieces[index ]["hasSwitched"] = nil
		if index == #_TrackPieces then
			index = 0
		end

		if _SwitchesCommands[index + 1] ~= nil then
			
			if _TrackPieces[index + 1]["hasSwitched"] == nil then
				local lane = _CarInfo["endLane"] + 1

				if lane < #_TrackLanes and _SwitchesCommands[index + 1][lane] > _SwitchesCommands[index + 1][lane+1] then
			
					self:send(self:switchLane("Right"))
					_TrackPieces[index + 1]["hasSwitched"] = true
					print("switch right")
					return
				end

				if   lane > 1 and _SwitchesCommands[index + 1][lane] > _SwitchesCommands[index + 1][lane-1] then 
					self:send(self:switchLane("Left"))
					_TrackPieces[index + 1]["hasSwitched"] = true
					print("switch left")
					return
				end
			end
		end
end


function NoobBot:GetStraightLineDistance()

	local index = _CarInfo["pieceIndex"] + 1
	local straightLineDistance = 0	
	
	-- check how much you have to travel until  curve
	while _TrackPieces[index]["length"] ~= nil do
		straightLineDistance =  straightLineDistance + _TrackPieces[index]["length"]
	
		index = index + 1
		if index > #_TrackPieces then
			index = 1
		end
	end

	local curveIndex = index


   if straightLineDistance > 0 and straightLineDistance > _CarInfo["inPieceDistance"]  then
		straightLineDistance = (straightLineDistance - _CarInfo["inPieceDistance"])
	end

	return straightLineDistance,curveIndex

end

function NoobBot:getDesiredSpeed( curveIndex )

	local radius = _TrackPieces[curveIndex]["radius"] 
	local angle = _TrackPieces[curveIndex]["angle"] 
	local lane = _CarInfo["endLane"] + 1
	
	if  angle > 0 then
		radius  = radius + _TrackLanes[lane]["distanceFromCenter"]	
	else
		radius  = radius - _TrackLanes[lane]["distanceFromCenter"]	
	end


	local desiredSpeed = (1- math.abs(angle)/100) + radius/1000
	return  desiredSpeed
end

function NoobBot:oncarpositions(data)
	
	
--	update car info
	self:updateCarInfo(data)
	local index = _CarInfo["pieceIndex"] + 1

--	calculate car speed
	self:UpdateCarSpeed()

--	calculate straight distance
	local straightLineDistance, curveIndex = self:GetStraightLineDistance()


--	 try to switch lane until curve
	self:SwitchLaneIfCase(curveIndex)

--	calculate desired speed for curve
	if straightLineDistance <= 0 then
		curveIndex = index
	end
	local desiredSpeed = self:getDesiredSpeed(curveIndex)


	
	local throttle = 0
	local startCoef =  desiredSpeed  - (speed  / 10)
	print("StartCoef "..startCoef)
	
   if straightLineDistance > 0 then
		straightLineDistance = straightLineDistance/ 100 
   
		desiredSpeed = desiredSpeed + straightLineDistance/10 
		print("DesiredSpeed "..desiredSpeed)
	
		startCoef = desiredSpeed  - (speed  / 10)
   
		if  turboAvailable  and  straightLineDistance > (turboFactor ) and _CarInfo["lap"]  == 2 and index > #_TrackPieces - 10 then
			turboAvailable = false
			startCoef = startCoef - 0.15 * turboFactor
			self:send(self:msg("turbo","GO GO GO"))
			return
		end
	
		if straightLineDistance < 1.7  then
			desiredSpeed = desiredSpeed -   0.35 * math.min(3,1/(straightLineDistance))
		end

		throttle = (  3 * startCoef + desiredSpeed   )
		throttle = math.min(1, throttle)

		print("ThrottleL: "..throttle)
	else
		print("DesiredSpeed "..desiredSpeed)
	
		
		throttle = ( 3 *startCoef + desiredSpeed  )
		
		local sign = startCoef/math.abs(startCoef)
		local overload = (speed/10 + sign * throttle/10) - desiredSpeed
		if overload then
			throttle = throttle - overload	
		end
		throttle = math.min(1, throttle)
		print("ThrottleC: "..throttle)
	end
	throttle = math.max(0.1,throttle)
	self:send(self:throttle(throttle))
	
end


turboAvailable = false
turboDurationMilliseconds = false
turboDurationTicks = false
turboFactor = false
function NoobBot:onturboavailable(data)
	turboAvailable = true
	turboDurationMilliseconds = data["turboDurationMilliseconds"]
	turboDurationTicks = data["turboDurationTicks"]
	turboFactor = data["turboFactor"]
	print("Yeeah baby")
end

function NoobBot:onyourcar(data)
	print("Got Car info")
	_Car = data
	table.foreach(_Car,PrintTable)		
end
function NoobBot:ongameinit(data)
	print("Got Game info")
	_Game = data
	_TrackLanes = _Game["race"]["track"]["lanes"]
	_TrackPieces = _Game["race"]["track"]["pieces"]

	FindSwitches()


	-- table.foreach(_Game)	
	-- table.foreach(_TrackLanes,PrintTable)	
	-- table.foreach(_TrackPieces,PrintTable)	
	-- table.foreach(_SwitchesCommands,PrintTable)	
end

function GetArcLength(angle, radius, laneIndex)

	if angle > 0 then
		radius = radius - _TrackLanes[laneIndex]["distanceFromCenter"]
	else
		radius = radius + _TrackLanes[laneIndex]["distanceFromCenter"]
	end
	

	local	arcLength =    0.0174532925 *  math.abs(angle) * radius 
	return arcLength
end



function GetDistanceToNextSwitch(switchIndex, laneIndex)
	local length = 0
	switchIndex = switchIndex + 1
	while _TrackPieces[switchIndex]["switch"] == nil do
	
		if _TrackPieces[switchIndex]["angle"] then
			length = length + GetArcLength(_TrackPieces[switchIndex]["angle"], _TrackPieces[switchIndex]["radius"], laneIndex)
		
		else
			length = _TrackPieces[switchIndex]["length"]
		end
		
		switchIndex = switchIndex + 1
		if switchIndex > #_TrackPieces then
			switchIndex = 1
		end
	end
	return length
end

function FindSwitches()
	_SwitchesCommands = {}
	local i = 1
	while i <= #_TrackPieces do 
		if _TrackPieces[i]["switch"] ~= nil then
			_SwitchesCommands[i] = {} --init table
			for lane=1, #_TrackLanes do
				_SwitchesCommands[i][lane] = GetDistanceToNextSwitch(i, lane)
			end
		end
		i = i+1
	end	
end
--****************************************************************


function NoobBot:msgloop()
   local line = self.conn:receive("*l")
   while line ~= nil  do
      LOGD("Got message: %s", line)
      local msg = json.decode(line)
     if msg then
		 local msgtype = msg["msgType"]
		_gameTick = msg["gameTick"]
		 local fn = NoobBot["on" .. string.lower(msgtype)]
		 if fn then
			fn(self, msg["data"])
		 else
			print("Got " .. msgtype)
			self:send(self:ping())
		 end
		  end
		  line = self.conn:receive("*l")
   end
end

if #arg == 4 then
   local host, port, name, key = unpack(arg)
   print("Connecting with parameters:")
   print(string.format("host=%s, port=%s, bot name=%s, key=%s", unpack(arg)))
   local c = assert(socket.connect(host, port))
   bot = NoobBot.create(c, name, key, "keimola")
   bot:run()
else
   print("Usage: ./run host port botname botkey")
end
